# Debconf translations for sn.
# Copyright (C) 2012 THE sn'S COPYRIGHT HOLDER
# This file is distributed under the same license as the sn package.
# Adriano Rafael Gomes <adrianorg@gmail.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: sn 0.3.8-10\n"
"Report-Msgid-Bugs-To: chris@niekel.net\n"
"POT-Creation-Date: 2006-08-19 22:07+0200\n"
"PO-Revision-Date: 2012-08-19 22:46-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../sn.templates:1001
msgid "cron, ip-up, manually"
msgstr "cron, ip-up, manualmente"

#. Type: select
#. Description
#: ../sn.templates:1002
msgid "sn should run from:"
msgstr "O sn deverá executar a partir de:"

#. Type: select
#. Description
#: ../sn.templates:1002
msgid ""
"The scripts provided with the package support several ways to run snget (the "
"program to fetch new news):"
msgstr ""
"Os scripts fornecidos com o pacote oferecem suporte a diversas formas de "
"executar o snget (o programa para buscar novas notícias):"

#. Type: select
#. Description
#: ../sn.templates:1002
msgid ""
" cron     -- The program will be executed daily by cron -- useful e.g\n"
"             for permanent connections;\n"
" ip-up    -- The program will called from ip-up, that is, when your\n"
"             computer makes a connection -- useful for e.g. dialup\n"
"             connections;\n"
" manually -- The program will never be called, you have to call it\n"
"             manually to get new news (just type snget as root)."
msgstr ""
" cron        -- O programa será executado diariamente pelo cron -- útil,\n"
"                por exemplo, para conexões permanentes;\n"
" ip-up       -- O programa será chamado a partir do ip-up, ou seja,\n"
"                quando o seu computador fizer uma conexão -- útil, por\n"
"                exemplo, para conexões discadas;\n"
" manualmente -- O programa nunca será chamado, você deverá chamá-lo\n"
"                manualmente para obter novas notícias (apenas digite\n"
"                snget como root)."

#. Type: boolean
#. Description
#: ../sn.templates:2001
msgid "Should sn only accept connections from localhost?"
msgstr "O sn deverá aceitar conexões somente a partir do localhost?"

#. Type: boolean
#. Description
#: ../sn.templates:2001
msgid ""
"sn is a small newsserver, intended mainly to be run for single user "
"systems.  On such systems, it's better to have sn only answer connections "
"from localhost.  If you intend to use sn from multiple machines, refuse here."
msgstr ""
"O sn é um pequeno servidor de notícias (\"newsserver\"), projetado para ser "
"executado principalmente em sistemas com um único usuário. Em tais sistemas, "
"é melhor deixar o sn responder somente conexões vindas do localhost. Se você "
"pretende usar o sn a partir de múltiplas máquinas, recuse aqui."
